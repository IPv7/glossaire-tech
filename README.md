# Glossaire technique

Un glossaire de mots techniques avec des définitions courtes (<150 caractères si possible) en français.

Il a été initialement créé pour la rédaction d’articles qui parlent de culture libre à destination du grand public, mais peut éventuellement servir à d’autres occasions.

## Comment l’utiliser ?

Accéder [au fichier glossaire](./glossaire_tech.yml).

Le moyen le moins intrusif d’ajouter une définition sur une page html est d’utiliser la balise `abbr`. Par exemple :

```
En somme, le meilleur moyen de lutter contre la centralisation
d’Internet dans d’immenses silos à données que sont les
<abbr title="Google, Amazon, Facebook, Apple et Microsoft.">GAFAM</abbr>,
serait de faire des silos plus petits.
```

Le fichier étant formaté en [Yaml](https://fr.wikipedia.org/wiki/YAML), il peut facilement être utilisé dans un script pour par exemple détecter les mots trop techniques dans un texte ou même ajouter la balise `abbr` automagiquement.

## Comment contribuer ?

Connectez-vous sur Framagit, puis cliquez sur le lien ci-dessus et cliquez ensuite sur le bouton *Éditer*, ce qui a pour effet de forker le dépôt et de créer une pull-request.

Si vous avez la flemme, vous pouvez aussi proposer des suggestions en [ouvrant un ticket](https://framagit.org/roipoussiere/glossaire-tech/issues/new), ou bien [me contacter sur Mastodon](https://mastodon.tetaneutral.net/@roipoussiere).

## Licence

L'ensemble des définitions est sous domaine public, il n'est donc pas nécessaire de demander l'autorisation pour l'utiliser.
